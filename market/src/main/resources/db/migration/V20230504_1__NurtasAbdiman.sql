create table users
(
    id       bigserial primary key,
    name     varchar(255) not null,
    surname  varchar(255),
    login    varchar(255) not null unique,
    password varchar(255) not null
);

create table product_types
(
    id    bigserial primary key,
    title varchar(255) not null unique
);

create table products
(
    id           bigserial primary key,
    title        varchar(255) not null,
    description  text         not null,
    product_type bigint       not null,
    image        varchar(255) not null,
    price        float        not null,
    count        bigint       not null,
    foreign key (product_type) references product_types (id)
);

create table baskets
(
    id         bigserial primary key,
    product_id bigint not null,
    user_id    bigint not null,
    count      bigint not null,
    foreign key (product_id) references products (id),
    foreign key (user_id) references users (id)

);

create table reviews
(
    id          bigserial primary key,
    user_id     bigint not null,
    review_text text   not null,
    product_id  bigint not null,
    foreign key (user_id) references users (id),
    foreign key (product_id) references products (id)
);

create table purchases
(
    id               bigserial primary key,
    user_id          bigint       not null,
    delivery_address varchar(255) not null,
    delivery_date    timestamp    not null,
    foreign key (user_id) references users (id)
);

create table purchase_infos
(
    id          bigserial primary key,
    purchase_id bigint not null,
    product_id  bigint not null,
    price       float  not null,
    count       bigint not null,
    foreign key (purchase_id) references purchases (id),
    foreign key (product_id) references products (id)
);
