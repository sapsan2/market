create table authorities
(
    id    bigserial primary key,
    title varchar(255) not null
);

create table roles_and_authorities
(
    role_id bigint not null,
    authority_id bigint not null
);