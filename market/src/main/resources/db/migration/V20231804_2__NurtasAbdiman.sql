create table roles
(
    id    bigserial primary key,
    title varchar(255) not null
);

create table users_and_roles
(
    user_id bigint not null,
    role_id bigint not null
);