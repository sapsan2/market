package com.example.market.mapper;

import com.example.market.dto.response.PurchaseDtoResponse;
import com.example.market.model.Purchase;

public class PurchaseMapper {

    public static PurchaseDtoResponse purchaseToDto(Purchase purchase) {

        PurchaseDtoResponse purchaseDtoResponse = new PurchaseDtoResponse();

        purchaseDtoResponse.setId(purchase.getId());
        purchaseDtoResponse.setUser(purchase.getUser());
        purchaseDtoResponse.setDeliveryAddress(purchase.getDeliveryAddress());
        purchaseDtoResponse.setDeliveryDate(purchase.getDeliveryDate());

        return purchaseDtoResponse;
    }
}
