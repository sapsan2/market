package com.example.market.mapper;

import com.example.market.dto.response.ReviewDtoResponse;
import com.example.market.model.Review;

public class ReviewMapper {

    public static ReviewDtoResponse reviewToDto(Review review) {

        ReviewDtoResponse reviewDtoResponse = new ReviewDtoResponse();

        reviewDtoResponse.setId(review.getId());
        reviewDtoResponse.setProduct(review.getProduct());
        reviewDtoResponse.setUser(review.getUser());
        reviewDtoResponse.setReviewText(review.getReviewText());

        return reviewDtoResponse;
    }
}
