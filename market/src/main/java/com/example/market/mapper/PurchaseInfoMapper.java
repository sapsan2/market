package com.example.market.mapper;

import com.example.market.dto.response.PurchaseInfoDtoResponse;
import com.example.market.model.PurchaseInfo;

public class PurchaseInfoMapper {

    public static PurchaseInfoDtoResponse purchaseInfoToDto(PurchaseInfo purchaseInfo) {

        PurchaseInfoDtoResponse purchaseInfoDtoResponse = new PurchaseInfoDtoResponse();

        purchaseInfoDtoResponse.setId(purchaseInfo.getId());
        purchaseInfoDtoResponse.setPurchase(purchaseInfo.getPurchase());
        purchaseInfoDtoResponse.setProduct(purchaseInfo.getProduct());
        purchaseInfoDtoResponse.setPrice(purchaseInfo.getPrice());
        purchaseInfoDtoResponse.setCount(purchaseInfo.getCount());

        return purchaseInfoDtoResponse;
    }
}
