package com.example.market.mapper;

import com.example.market.dto.response.ProductTypeDtoResponse;
import com.example.market.model.ProductType;

public class ProductTypeMapper {
    public static ProductTypeDtoResponse productTypeToDto(ProductType productType) {
        ProductTypeDtoResponse productTypeDtoResponse = new ProductTypeDtoResponse();

        productTypeDtoResponse.setId(productType.getId());
        productTypeDtoResponse.setTitle(productType.getTitle());
        return productTypeDtoResponse;
    }
}
