package com.example.market.mapper;

import com.example.market.dto.response.UserDtoResponse;
import com.example.market.model.security.User;

public class UserMapper {

    public static UserDtoResponse userToDto(User user) {
        UserDtoResponse userDtoResponse = new UserDtoResponse();

        userDtoResponse.setLogin(user.getLogin());
        userDtoResponse.setName(user.getName());

        return userDtoResponse;
    }
}
