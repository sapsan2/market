package com.example.market.mapper;

import com.example.market.dto.response.BasketDtoResponse;
import com.example.market.model.Basket;

public class BasketMapper {

    public static BasketDtoResponse basketToDto(Basket basket) {
        BasketDtoResponse basketDtoResponse = new BasketDtoResponse();

        basketDtoResponse.setId(basket.getId());
        basketDtoResponse.setProduct(basket.getProduct());
        basketDtoResponse.setUser(basket.getUser());
        basketDtoResponse.setCount(basket.getCount());

        return basketDtoResponse;
    }
}
