package com.example.market.mapper;

import com.example.market.dto.response.ProductDtoResponse;
import com.example.market.model.Product;

public class ProductMapper {

    public static ProductDtoResponse productToDto(Product product) {
        ProductDtoResponse productDtoResponse = new ProductDtoResponse();

        productDtoResponse.setId(product.getId());
        productDtoResponse.setProductType(product.getProductType());
        productDtoResponse.setTitle(product.getTitle());
        productDtoResponse.setDescription(product.getDescription());
        productDtoResponse.setImage(product.getImage());
        productDtoResponse.setPrice(product.getPrice());
        productDtoResponse.setCount(product.getCount());

        return productDtoResponse;
    }
}
