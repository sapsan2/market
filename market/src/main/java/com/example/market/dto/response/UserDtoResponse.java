package com.example.market.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDtoResponse {

    private String login;

    private String name;
}
