package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class ReviewDtoRequest {

    @NotNull
    private Long productId;

    @NotNull
    private Long userId;

    @NotBlank(message = "Тест отзыва должен быть указан.")
    private String reviewText;
}
