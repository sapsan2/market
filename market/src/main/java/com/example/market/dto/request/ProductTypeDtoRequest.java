package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class ProductTypeDtoRequest {

    @NotBlank(message = "Тип должен быть указан.")
    private String title;
}
