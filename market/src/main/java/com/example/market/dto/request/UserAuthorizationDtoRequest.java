package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class UserAuthorizationDtoRequest {

    @NotBlank(message = "Логин должен быть указан.")
    private String login;

    @NotBlank(message = "Пароль должен быть указан.")
    private String password;

}
