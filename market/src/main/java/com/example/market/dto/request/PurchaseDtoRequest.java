package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class PurchaseDtoRequest {

    @NotNull
    private Long userId;

    @NotBlank(message = "Адрес доставки должен быть указан.")
    private String deliveryAddress;

    @NotBlank(message = "Дата доставки должна быть указана.")
    private String deliveryDate;
}
