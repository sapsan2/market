package com.example.market.dto.response;

import com.example.market.model.Product;
import com.example.market.model.security.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewDtoResponse {

    private Long id;

    private Product product;

    private User user;

    private String reviewText;
}
