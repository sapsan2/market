package com.example.market.dto.response;

import com.example.market.model.ProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDtoResponse {

    private Long id;

    private ProductType productType;

    private String title;

    private String description;

    private String image;

    private Long price;

    private Long count;
}
