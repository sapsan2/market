package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PurchaseInfoDtoRequest {

    @NotNull
    private Long purchaseId;

    @NotNull
    private Long productId;

    @NotNull
    private Long price;

    @NotNull
    private Long count;
}
