package com.example.market.dto.response;

import com.example.market.model.security.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseDtoResponse {
    private Long id;

    private User user;

    private String deliveryAddress;

    private String deliveryDate;
}
