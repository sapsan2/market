package com.example.market.dto.response;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class ProductTypeDtoResponse {

    private Long id;

    private String title;
}
