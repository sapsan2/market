package com.example.market.dto.response;

import com.example.market.model.Product;
import com.example.market.model.Purchase;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchaseInfoDtoResponse {

    private Long id;

    private Purchase purchase;

    private Product product;

    private Long price;

    private Long count;
}
