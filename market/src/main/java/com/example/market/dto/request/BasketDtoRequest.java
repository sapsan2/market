package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class BasketDtoRequest {

    @NotNull
    private Long productId;

    @NotNull
    private Long userId;

    @NotNull
    private Long count;
}
