package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class ProductDtoRequest {

    @NotNull
    private Long productTypeId;

    @NotBlank(message = "Название продукта должно быть указано.")
    private String title;

    @NotBlank(message = "Описание продукта должно быть указано.")
    private String description;

    @NotBlank(message = "Изображение продукта должно быть указано.")
    private String image;

    @NotNull
    private Long price;

    @NotNull
    private Long count;
}
