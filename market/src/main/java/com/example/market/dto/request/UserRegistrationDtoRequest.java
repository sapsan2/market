package com.example.market.dto.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class UserRegistrationDtoRequest {

    @NotBlank(message = "Логин должен быть указан.")
    private String login;

    @NotBlank(message = "Пароль должен быть указан.")
    private String password;

    @NotBlank(message = "Имя должно быть указано.")
    private String name;
}
