package com.example.market.controller;

import com.example.market.dto.request.UserAuthorizationDtoRequest;
import com.example.market.dto.request.UserRegistrationDtoRequest;
import com.example.market.dto.response.UserDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController extends ExceptionHandling {

    private final UserService userService;

    @PostMapping("/registration")
    public ResponseEntity<HttpStatus> registration(@Valid @RequestBody UserRegistrationDtoRequest dtoRequest) {
        userService.registration(dtoRequest);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/authorization")
    public ResponseEntity<UserDtoResponse> authorization(@Valid @RequestBody UserAuthorizationDtoRequest dtoRequest,
                                                         HttpServletRequest request) {
        return userService.authorization(dtoRequest, request);
    }

}
