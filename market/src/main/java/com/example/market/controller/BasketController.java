package com.example.market.controller;

import com.example.market.dto.request.BasketDtoRequest;
import com.example.market.dto.response.BasketDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.BasketMapper;
import com.example.market.model.Basket;
import com.example.market.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/basket")
@RequiredArgsConstructor
public class BasketController extends ExceptionHandling {

    private final BasketService basketService;

    @PreAuthorize("hasAnyAuthority('/basket/get-id/')")
    @GetMapping("/getById/{id}")
    public ResponseEntity<Basket> getByBasketId(@Valid @PathVariable(name = "id") Long id) {
        Basket basket = basketService.getByIdThrowException(id);

        return new ResponseEntity<>(basket, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/basket/create')")
    @PostMapping("/create")
    public ResponseEntity<BasketDtoResponse> create(@Valid @RequestBody BasketDtoRequest dtoRequest) {
        Basket basket = basketService.create(dtoRequest);

        BasketDtoResponse basketDtoResponse = BasketMapper.basketToDto(basket);

        return new ResponseEntity<>(basketDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/basket/update/')")
    @PostMapping("/update/{id}")
    public ResponseEntity<BasketDtoResponse> update(@Valid @RequestBody BasketDtoRequest dtoRequest,
                                                    @PathVariable(name = "id") Long id) {
        Basket basket = basketService.update(dtoRequest, id);

        BasketDtoResponse basketDtoResponse = BasketMapper.basketToDto(basket);

        return new ResponseEntity<>(basketDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/basket/delete/')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        basketService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
