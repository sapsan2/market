package com.example.market.controller;

import com.example.market.dto.request.ProductTypeDtoRequest;
import com.example.market.dto.response.ProductTypeDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.ProductTypeMapper;
import com.example.market.model.ProductType;
import com.example.market.service.ProductTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/product-type")
@RequiredArgsConstructor
public class ProductTypeController extends ExceptionHandling {

    private final ProductTypeService productTypeService;

    @PreAuthorize("hasAnyAuthority('/product-type/get-id/')")
    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<ProductType> getByProductTypeId(@Valid @PathVariable(name = "id") Long id) {
        ProductType productType = productTypeService.getByIdThrowException(id);

        return new ResponseEntity<>(productType, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/product-type/create')")
    @PostMapping("/create")
    public ResponseEntity<ProductTypeDtoResponse> create(@Valid @RequestBody ProductTypeDtoRequest dtoRequest) {
        ProductType productType = productTypeService.create(dtoRequest);

        ProductTypeDtoResponse productTypeDtoResponse = ProductTypeMapper.productTypeToDto(productType);

        return new ResponseEntity<>(productTypeDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/product-type/update/')")
    @PostMapping("/update/{id}")
    public ResponseEntity<ProductTypeDtoResponse> create(@Valid @RequestBody ProductTypeDtoRequest dtoRequest,
                                                         @PathVariable(name = "id") Long id) {
        ProductType productType = productTypeService.update(dtoRequest, id);

        ProductTypeDtoResponse productTypeDtoResponse = ProductTypeMapper.productTypeToDto(productType);

        return new ResponseEntity<>(productTypeDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/product-type/delete/')")
    @PostMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        productTypeService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
