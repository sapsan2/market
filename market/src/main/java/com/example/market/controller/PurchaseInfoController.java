package com.example.market.controller;

import com.example.market.dto.request.PurchaseInfoDtoRequest;
import com.example.market.dto.response.PurchaseInfoDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.PurchaseInfoMapper;
import com.example.market.model.PurchaseInfo;
import com.example.market.service.PurchaseInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/purchase-info")
@RequiredArgsConstructor
public class PurchaseInfoController extends ExceptionHandling {

    private final PurchaseInfoService purchaseInfoService;

    @PreAuthorize("hasAnyAuthority('/purchase-info/get-id/')")
    @GetMapping("/get-id/{id}")
    public ResponseEntity<PurchaseInfo> getByPurchaseInfoId(@Valid @PathVariable(name = "id") Long id) {
        PurchaseInfo purchaseInfo = purchaseInfoService.getByIdThrowException(id);

        return new ResponseEntity<>(purchaseInfo, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/purchase-info/create')")
    @PostMapping("/create")
    public ResponseEntity<PurchaseInfoDtoResponse> create(@Valid @RequestBody PurchaseInfoDtoRequest dtoRequest) {
        PurchaseInfo purchaseInfo = purchaseInfoService.create(dtoRequest);

        PurchaseInfoDtoResponse purchaseInfoDtoResponse = PurchaseInfoMapper.purchaseInfoToDto(purchaseInfo);

        return new ResponseEntity<>(purchaseInfoDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/purchase-info/update/')")
    @PostMapping("/update")
    public ResponseEntity<PurchaseInfoDtoResponse> update(@Valid @RequestBody PurchaseInfoDtoRequest dtoRequest,
                                                          @PathVariable(name = "id") Long id) {
        PurchaseInfo purchaseInfo = purchaseInfoService.update(dtoRequest, id);

        PurchaseInfoDtoResponse purchaseInfoDtoResponse = PurchaseInfoMapper.purchaseInfoToDto(purchaseInfo);

        return new ResponseEntity<>(purchaseInfoDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/purchase-info/delete/')")
    @PostMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        purchaseInfoService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
