package com.example.market.controller;

import com.example.market.dto.request.ProductDtoRequest;
import com.example.market.dto.response.ProductDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.ProductMapper;
import com.example.market.model.Product;
import com.example.market.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductController extends ExceptionHandling {

    private final ProductService productService;

    @GetMapping("/getAll")
    public ResponseEntity<List<Product>> getAll() {
        List<Product> productList = productService.getAll();

        return new ResponseEntity<>(productList, HttpStatus.OK);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<Product> getByProductId(@Valid @PathVariable(name = "id") Long id) {
        Product product = productService.getByIdThrowException(id);

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/product/create')")
    @PostMapping("/create")
    public ResponseEntity<ProductDtoResponse> create(@Valid @RequestBody ProductDtoRequest dtoRequest) {
        Product product = productService.create(dtoRequest);

        ProductDtoResponse productDtoResponse = ProductMapper.productToDto(product);

        return new ResponseEntity<>(productDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/product/update/')")
    @PostMapping("/update/{id}")
    public ResponseEntity<ProductDtoResponse> update(@Valid @RequestBody ProductDtoRequest dtoRequest,
                                                     @PathVariable(name = "id") Long id) {
        Product product = productService.update(dtoRequest, id);

        ProductDtoResponse productDtoResponse = ProductMapper.productToDto(product);

        return new ResponseEntity<>(productDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/product/delete/')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        productService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/product/product-title/')")
    @GetMapping("/product-title")
    public ResponseEntity<List<Product>> getByTitle(@RequestParam("title") String title) {

        List<Product> productList = productService.getByTitle(title);

        return new ResponseEntity<>(productList, HttpStatus.OK);
    }
}
