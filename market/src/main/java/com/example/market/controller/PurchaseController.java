package com.example.market.controller;

import com.example.market.dto.request.PurchaseDtoRequest;
import com.example.market.dto.response.PurchaseDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.PurchaseMapper;
import com.example.market.model.Purchase;
import com.example.market.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/purchase")
@RequiredArgsConstructor
public class PurchaseController extends ExceptionHandling {

    private final PurchaseService purchaseService;

    @PreAuthorize("hasAnyAuthority('/purchase/getById/')")
    @GetMapping("/getById/{id}")
    public ResponseEntity<Purchase> getByPurchaseId(@Valid @PathVariable(name = "id") Long id) {
        Purchase Purchase = purchaseService.getByIdThrowException(id);

        return new ResponseEntity<>(Purchase, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/purchase/create')")
    @PostMapping("/create")
    public ResponseEntity<PurchaseDtoResponse> create(@Valid @RequestBody PurchaseDtoRequest dtoRequest) {
        Purchase purchase = purchaseService.create(dtoRequest);

        PurchaseDtoResponse purchaseDtoResponse = PurchaseMapper.purchaseToDto(purchase);

        return new ResponseEntity<>(purchaseDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/purchase/update/')")
    @PostMapping("/update/{id}")
    public ResponseEntity<PurchaseDtoResponse> update(@Valid @RequestBody PurchaseDtoRequest dtoRequest,
                                                      @PathVariable(name = "id") Long id) {
        Purchase purchase = purchaseService.update(dtoRequest, id);

        PurchaseDtoResponse purchaseDtoResponse = PurchaseMapper.purchaseToDto(purchase);

        return new ResponseEntity<>(purchaseDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/purchase/delete/')")
    @PostMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        purchaseService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
