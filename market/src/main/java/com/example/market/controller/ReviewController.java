package com.example.market.controller;

import com.example.market.dto.request.ReviewDtoRequest;
import com.example.market.dto.response.ReviewDtoResponse;
import com.example.market.exception.ExceptionHandling;
import com.example.market.mapper.ReviewMapper;
import com.example.market.model.Review;
import com.example.market.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/review")
@RequiredArgsConstructor
public class ReviewController extends ExceptionHandling {

    private final ReviewService reviewService;

    @PreAuthorize("hasAnyAuthority('/review/getById/')")
    @GetMapping("/getById/{id}")
    public ResponseEntity<Review> getReviewById(@Valid @PathVariable(name = "id") Long id) {
        Review review = reviewService.getByIdThrowException(id);

        return new ResponseEntity<>(review, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/review/create')")
    @PostMapping("/create")
    public ResponseEntity<ReviewDtoResponse> create(@Valid @RequestBody ReviewDtoRequest dtoRequest) {
        Review review = reviewService.create(dtoRequest);

        ReviewDtoResponse reviewDtoResponse = ReviewMapper.reviewToDto(review);

        return new ResponseEntity<>(reviewDtoResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('/review/update/')")
    @PostMapping("/update/{id}")
    public ResponseEntity<ReviewDtoResponse> update(@Valid @RequestBody ReviewDtoRequest dtoRequest,
                                                    @PathVariable(name = "id") Long id) {
        Review review = reviewService.update(dtoRequest, id);

        ReviewDtoResponse reviewDtoResponse = ReviewMapper.reviewToDto(review);

        return new ResponseEntity<>(reviewDtoResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('/review/delete/')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable(name = "id") Long id) {
        reviewService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
