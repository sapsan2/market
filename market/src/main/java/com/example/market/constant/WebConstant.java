package com.example.market.constant;

public class WebConstant {

    public final static String[] PERMIT_ALL_URL = {"/api/v1/user/registration", "/api/v1/user/authorization"};
    public final static String[] PERMIT_ALL_PRODUCT_URL = {"/api/v1/product/getAll", "/api/v1/product/getById/**"};
}
