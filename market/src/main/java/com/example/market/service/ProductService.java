package com.example.market.service;

import com.example.market.dto.request.ProductDtoRequest;
import com.example.market.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> getById(Long id);

    Product getByIdThrowException(Long id);

    List<Product> getAll();

    Product create(ProductDtoRequest dtoRequest);

    Product update(ProductDtoRequest dtoRequest, Long id);

    void delete(Long id);

    List<Product> getByTitle(String title);
}
