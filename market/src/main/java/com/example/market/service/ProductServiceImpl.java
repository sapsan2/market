package com.example.market.service;

import com.example.market.dto.request.ProductDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryDeleteException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.Product;
import com.example.market.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final ProductTypeService productTypeService;

    @Override
    public Optional<Product> getById(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public Product getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    private Product getProduct(ProductDtoRequest dtoRequest, Product product) {
        product.setProductType(productTypeService.getByIdThrowException(dtoRequest.getProductTypeId()));
        product.setTitle(dtoRequest.getTitle());
        product.setDescription(dtoRequest.getDescription());
        product.setImage(dtoRequest.getImage());
        product.setPrice(dtoRequest.getPrice());
        product.setCount(dtoRequest.getCount());

        return productRepository.save(product);
    }

    @Override
    public Product create(ProductDtoRequest dtoRequest) {

        Product product = new Product();

        try {
            return getProduct(dtoRequest, product);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public Product update(ProductDtoRequest dtoRequest, Long id) {

        Product product = this.getByIdThrowException(id);

        try {
            return getProduct(dtoRequest, product);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public void delete(Long id) {

        Product product = this.getByIdThrowException(id);

        try {
            productRepository.delete(product);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryDeleteException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public List<Product> getByTitle(String title) {
        return productRepository.selectByTitle(title);
    }
}
