package com.example.market.service;

import com.example.market.dto.request.ProductDtoRequest;
import com.example.market.dto.request.PurchaseDtoRequest;
import com.example.market.model.Product;
import com.example.market.model.Purchase;

import java.util.Optional;

public interface PurchaseService {
    Optional<Purchase> getById(Long id);

    Purchase getByIdThrowException(Long id);

    Purchase create(PurchaseDtoRequest dtoRequest);

    Purchase update(PurchaseDtoRequest dtoRequest, Long id);

    void delete(Long id);

}
