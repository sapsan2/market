package com.example.market.service;

import com.example.market.dto.request.UserAuthorizationDtoRequest;
import com.example.market.dto.request.UserRegistrationDtoRequest;
import com.example.market.dto.response.UserDtoResponse;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.*;
import com.example.market.mapper.UserMapper;
import com.example.market.model.security.User;
import com.example.market.repository.UserRepository;
import com.example.market.security.JWTTokenProvider;
import com.example.market.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder encoder;

    private final AuthenticationManager authenticationManager;

    private final JWTTokenProvider jwtTokenProvider;

    private final String USER_LOGIN_ALREADY_EXIST = "Данный логин уже занят.";

    private final String AUTHENTICATION_EXCEPTION = "Логин или пароль указаны не верно.";

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.getByUserLoginThrowException(username);

        return new UserPrincipal(user);
    }

    @Override
    public Optional<User> getByUserLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User getByUserLoginThrowException(String login) {
        return this.getByUserLogin(login).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    @Override
    public Optional<User> getByUserId(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User getByUserIdThrowException(Long id) {
        return this.getByUserId(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    @Override
    public void registration(UserRegistrationDtoRequest dtoRequest) {

        String login = dtoRequest.getLogin().toLowerCase().trim();
        String password = dtoRequest.getPassword().trim();

       this.getByUserLogin(login)
                .ifPresent((user) -> {
                    throw new AlreadyExistException(this.USER_LOGIN_ALREADY_EXIST);
                });

        try {
            User createdUser = new User();

            createdUser.setLogin(login);
            createdUser.setPassword(encoder.encode(password));
            createdUser.setName(dtoRequest.getName());

            userRepository.save(createdUser);

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public ResponseEntity<UserDtoResponse> authorization(UserAuthorizationDtoRequest dtoRequest, HttpServletRequest request) {

        String login = dtoRequest.getLogin().toLowerCase().trim();
        String password = dtoRequest.getPassword().trim();

        try {
            this.authenticate(login, password);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomShowMessageException(this.AUTHENTICATION_EXCEPTION);
        }

        try {
            User user = this.getByUserLoginThrowException(login);
            UserPrincipal userPrincipal = new UserPrincipal(user);

            String IP = jwtTokenProvider.getIpFromClient(request);

            HttpHeaders httpHeaders = this.JWTHeader(userPrincipal, IP);

            return new ResponseEntity<>(UserMapper.userToDto(user), httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UnexpectedException(CustomExceptionMessage.UNEXPECTED_EXCEPTION_MESSAGE);
        }


    }

    private HttpHeaders JWTHeader(UserPrincipal userPrincipal, String IP) {
        HttpHeaders httpHeaders = new HttpHeaders();

        String JWT = jwtTokenProvider.generateToken(userPrincipal, IP);
        httpHeaders.add(HttpHeaders.AUTHORIZATION, JWT);

        return httpHeaders;
    }

    private void authenticate(String login, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));
    }


}
