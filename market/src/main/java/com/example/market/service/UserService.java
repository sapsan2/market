package com.example.market.service;

import com.example.market.dto.request.UserAuthorizationDtoRequest;
import com.example.market.dto.request.UserRegistrationDtoRequest;
import com.example.market.dto.response.UserDtoResponse;
import com.example.market.model.security.User;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface UserService {

    Optional<User> getByUserLogin(String login);

    User getByUserLoginThrowException(String login);

    Optional<User> getByUserId(Long id);

    User getByUserIdThrowException(Long id);

    void registration(UserRegistrationDtoRequest dtoRequest);

    ResponseEntity<UserDtoResponse> authorization(UserAuthorizationDtoRequest dtoRequest, HttpServletRequest request);
}
