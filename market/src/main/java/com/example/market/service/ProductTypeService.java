package com.example.market.service;

import com.example.market.dto.request.ProductTypeDtoRequest;
import com.example.market.model.ProductType;

import java.util.Optional;

public interface ProductTypeService {


    Optional<ProductType> getById(Long id);

    ProductType getByIdThrowException(Long id);

    ProductType create(ProductTypeDtoRequest dtoRequest);

    ProductType update(ProductTypeDtoRequest dtoRequest, Long id);

    void delete(Long id);

}
