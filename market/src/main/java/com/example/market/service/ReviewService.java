package com.example.market.service;

import com.example.market.dto.request.ReviewDtoRequest;
import com.example.market.model.Review;

import java.util.Optional;

public interface ReviewService {

    Optional<Review> getById(Long id);

    Review getByIdThrowException(Long id);

    Review create(ReviewDtoRequest dtoRequest);

    Review update(ReviewDtoRequest dtoRequest, Long id);

    void delete(Long id);
}
