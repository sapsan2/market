package com.example.market.service;

import com.example.market.dto.request.ReviewDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryDeleteException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.Review;
import com.example.market.repository.ReviewRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class ReviewServiceImpl implements ReviewService{

    private final ReviewRepository reviewRepository;

    private final ProductService productService;

    private final UserService userService;

    @Override
    public Optional<Review> getById(Long id) {
        return reviewRepository.findById(id);
    }

    @Override
    public Review getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    private Review getReview(ReviewDtoRequest dtoRequest, Review review) {

        review.setProduct(productService.getByIdThrowException(dtoRequest.getProductId()));
        review.setUser(userService.getByUserIdThrowException(dtoRequest.getUserId()));
        review.setReviewText(dtoRequest.getReviewText());

        return reviewRepository.save(review);
    }

    @Override
    public Review create(ReviewDtoRequest dtoRequest) {
        Review review = new Review();

        try {
            return getReview(dtoRequest, review);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public Review update(ReviewDtoRequest dtoRequest, Long id) {

        Review review = this.getByIdThrowException(id);

        try {
            return getReview(dtoRequest, review);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public void delete(Long id) {

        Review review = this.getByIdThrowException(id);

        try {
            reviewRepository.delete(review);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryDeleteException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }
    }
}
