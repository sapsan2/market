package com.example.market.service;

import com.example.market.dto.request.ProductTypeDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryDeleteException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.ProductType;
import com.example.market.repository.ProductTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class ProductTypeServiceImpl implements ProductTypeService {

    private final ProductTypeRepository productTypeRepository;

    @Override
    public Optional<ProductType> getById(Long id) {
        return productTypeRepository.findById(id);
    }

    @Override
    public ProductType getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    private ProductType getProductType(ProductTypeDtoRequest dtoRequest, ProductType productType) {

        productType.setTitle(dtoRequest.getTitle());

        return productTypeRepository.save(productType);
    }

    @Override
    public ProductType create(ProductTypeDtoRequest dtoRequest) {
        ProductType productType = new ProductType();

        try {
            return getProductType(dtoRequest, productType);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public ProductType update(ProductTypeDtoRequest dtoRequest, Long id) {
        ProductType productType = this.getByIdThrowException(id);

        try {
            return getProductType(dtoRequest, productType);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public void delete(Long id) {

        ProductType productType = this.getByIdThrowException(id);

        try {
            productTypeRepository.delete(productType);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryDeleteException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }

    }
}
