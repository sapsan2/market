package com.example.market.service;

import com.example.market.dto.request.BasketDtoRequest;
import com.example.market.model.Basket;

import java.util.Optional;

public interface BasketService {
    Optional<Basket> getById(Long id);

    Basket getByIdThrowException(Long id);

    Basket create(BasketDtoRequest dtoRequest);

    Basket update(BasketDtoRequest dtoRequest, Long id);

    void delete(Long id);
}
