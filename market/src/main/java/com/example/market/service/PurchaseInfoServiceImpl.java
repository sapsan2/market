package com.example.market.service;

import com.example.market.dto.request.PurchaseInfoDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.PurchaseInfo;
import com.example.market.repository.PurchaseInfoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class PurchaseInfoServiceImpl implements PurchaseInfoService {

    private final PurchaseInfoRepository purchaseInfoRepository;

    private final PurchaseService purchaseService;

    private final ProductService productService;

    @Override
    public Optional<PurchaseInfo> getById(Long id) {
        return purchaseInfoRepository.findById(id);
    }

    @Override
    public PurchaseInfo getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    private PurchaseInfo getPurchaseInfo(PurchaseInfoDtoRequest dtoRequest, PurchaseInfo purchaseInfo) {

        purchaseInfo.setPurchase(purchaseService.getByIdThrowException(dtoRequest.getPurchaseId()));
        purchaseInfo.setProduct(productService.getByIdThrowException(dtoRequest.getProductId()));
        purchaseInfo.setPrice(dtoRequest.getPrice());
        purchaseInfo.setCount(dtoRequest.getCount());

        return purchaseInfoRepository.save(purchaseInfo);
    }

    @Override
    public PurchaseInfo create(PurchaseInfoDtoRequest dtoRequest) {

        PurchaseInfo purchaseInfo = new PurchaseInfo();

        try {
            return getPurchaseInfo(dtoRequest, purchaseInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public PurchaseInfo update(PurchaseInfoDtoRequest dtoRequest, Long id) {

        PurchaseInfo purchaseInfo = this.getByIdThrowException(id);

        try {
            return getPurchaseInfo(dtoRequest, purchaseInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public void delete(Long id) {
        PurchaseInfo purchaseInfo = this.getByIdThrowException(id);

        try {
            purchaseInfoRepository.delete(purchaseInfo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }
    }
}
