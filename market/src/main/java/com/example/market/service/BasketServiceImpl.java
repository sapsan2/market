package com.example.market.service;

import com.example.market.dto.request.BasketDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryDeleteException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.Basket;
import com.example.market.repository.BasketRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;

    private final ProductService productService;

    private final UserService userService;

    @Override
    public Optional<Basket> getById(Long id) {
        return basketRepository.findById(id);
    }

    @Override
    public Basket getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    private Basket getBasket(BasketDtoRequest dtoRequest, Basket basket) {

        basket.setProduct(productService.getByIdThrowException(dtoRequest.getProductId()));
        basket.setUser(userService.getByUserIdThrowException(dtoRequest.getUserId()));
        basket.setCount(dtoRequest.getCount());

        return basketRepository.save(basket);
    }

    @Override
    public Basket create(BasketDtoRequest dtoRequest) {

        Basket basket = new Basket();
        try {
            return getBasket(dtoRequest, basket);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public Basket update(BasketDtoRequest dtoRequest, Long id) {

        Basket basket = this.getByIdThrowException(id);

        try {
            return getBasket(dtoRequest, basket);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }


    @Override
    public void delete(Long id) {
        Basket basket = this.getByIdThrowException(id);
        try {
            basketRepository.delete(basket);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryDeleteException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }
    }
}
