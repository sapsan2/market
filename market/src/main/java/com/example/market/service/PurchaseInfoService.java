package com.example.market.service;

import com.example.market.dto.request.PurchaseInfoDtoRequest;
import com.example.market.model.PurchaseInfo;

import java.util.Optional;

public interface PurchaseInfoService {

    Optional<PurchaseInfo> getById(Long id);

    PurchaseInfo getByIdThrowException(Long id);

    PurchaseInfo create(PurchaseInfoDtoRequest dtoRequest);

    PurchaseInfo update(PurchaseInfoDtoRequest dtoRequest, Long id);

    void delete(Long id);
}
