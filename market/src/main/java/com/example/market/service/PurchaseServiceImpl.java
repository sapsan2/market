package com.example.market.service;

import com.example.market.dto.request.PurchaseDtoRequest;
import com.example.market.exception.CustomExceptionMessage;
import com.example.market.exception.custom.NotFoundException;
import com.example.market.exception.custom.RepositoryCreateException;
import com.example.market.exception.custom.RepositoryDeleteException;
import com.example.market.exception.custom.RepositoryUpdateException;
import com.example.market.model.Purchase;
import com.example.market.repository.PurchaseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Log4j2
@RequiredArgsConstructor
public class PurchaseServiceImpl implements PurchaseService {

    private final PurchaseRepository purchaseRepository;

    private final UserService userService;

    @Override
    public Optional<Purchase> getById(Long id) {
        return purchaseRepository.findById(id);
    }

    @Override
    public Purchase getByIdThrowException(Long id) {
        return this.getById(id).orElseThrow(() -> new NotFoundException(CustomExceptionMessage.NOT_FOUND_EXCEPTION_MESSAGE));
    }

    private Purchase getPurchase(PurchaseDtoRequest dtoRequest, Purchase purchase) {

        purchase.setUser(userService.getByUserIdThrowException(dtoRequest.getUserId()));
        purchase.setDeliveryAddress(dtoRequest.getDeliveryAddress());
        purchase.setDeliveryDate(dtoRequest.getDeliveryDate());

        return purchaseRepository.save(purchase);
    }

    @Override
    public Purchase create(PurchaseDtoRequest dtoRequest) {

        Purchase purchase = new Purchase();

        try {
            return getPurchase(dtoRequest, purchase);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryCreateException(CustomExceptionMessage.CREATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public Purchase update(PurchaseDtoRequest dtoRequest, Long id) {

        Purchase purchase = this.getByIdThrowException(id);

        try {
            return getPurchase(dtoRequest, purchase);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryUpdateException(CustomExceptionMessage.UPDATE_EXCEPTION_MESSAGE);
        }
    }

    @Override
    public void delete(Long id) {

        Purchase purchase = this.getByIdThrowException(id);

        try {
            purchaseRepository.delete(purchase);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RepositoryDeleteException(CustomExceptionMessage.DELETE_EXCEPTION_MESSAGE);
        }
    }
}
