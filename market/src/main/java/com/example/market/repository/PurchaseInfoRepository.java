package com.example.market.repository;

import com.example.market.model.PurchaseInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseInfoRepository extends JpaRepository<PurchaseInfo, Long> {
}
