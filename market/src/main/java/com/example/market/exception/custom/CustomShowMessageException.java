package com.example.market.exception.custom;

public class CustomShowMessageException extends RuntimeException{
    public CustomShowMessageException(String message) {
        super(message);
    }
}
