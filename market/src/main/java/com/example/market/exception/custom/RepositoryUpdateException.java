package com.example.market.exception.custom;

public class RepositoryUpdateException extends RuntimeException{
    public RepositoryUpdateException(String message) {
        super(message);
    }
}
