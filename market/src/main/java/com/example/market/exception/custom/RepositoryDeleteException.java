package com.example.market.exception.custom;

public class RepositoryDeleteException extends RuntimeException{
    public RepositoryDeleteException(String message) {
        super(message);
    }
}
