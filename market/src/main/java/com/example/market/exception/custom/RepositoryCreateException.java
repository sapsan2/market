package com.example.market.exception.custom;

public class RepositoryCreateException extends RuntimeException{

    public RepositoryCreateException(String message) {
        super(message);
    }
}
